package com.ait.team;

///  http://viralpatel.net/blogs/java-read-write-excel-file-apache-poi/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omg.CORBA.PUBLIC_MEMBER;

public class ReadExcelData {

	public static void main(String[] args) {
		try {

			FileInputStream file = new FileInputStream(new File(
					"excel/test.xlsx"));

			// Get the workbook instance for XLS file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();

					switch (cell.getCellType()) {
						case Cell.CELL_TYPE_BOOLEAN:
							System.out.print(cell.getBooleanCellValue()
									+ "\t\t");
							break;
						case Cell.CELL_TYPE_NUMERIC:
							System.out.print(cell.getNumericCellValue()
									+ "\t\t");
							break;
						case Cell.CELL_TYPE_STRING:
							System.out
									.print(cell.getStringCellValue() + "\t\t");
							break;
					}
				}
				System.out.println("");
			}
			file.close();
			FileOutputStream out = new FileOutputStream(new File(
					"excel/csvtest.csv"));
			workbook.write(out);
			out.close();
			
			
			///////  put in String array and print out

			System.out.println();
			int rowNum = sheet.getLastRowNum() + 1;
			int colNum = sheet.getRow(0).getLastCellNum();
			String[][] data = new String[rowNum][colNum];

			for (int row = 0; row < rowNum; row++) {
				XSSFRow row2 = sheet.getRow(row);
				for (int col = 0; col < colNum; col++) {
					XSSFCell cell = row2.getCell(col);
					String value = getCelltoString(cell);
					data[row][col] = value;
					System.out.println("The value is: " + value);

				}
				System.out.println();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String getCelltoString(XSSFCell cell) {

		int type;
		Object result = null;
		type = cell.getCellType();

		switch (type) {
			case 0:
				result = cell.getNumericCellValue();
				break;
			case 1:
				result = cell.getStringCellValue();
				break;
			default:
				System.out.println("Wrong type");
				break;

		}
		return result.toString();

	}

}

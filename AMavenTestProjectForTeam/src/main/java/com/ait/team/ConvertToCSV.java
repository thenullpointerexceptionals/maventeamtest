package com.ait.team;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;


public class ConvertToCSV {
	private File outputCSVFile = new File("tempdata.csv");

	public File convertFile(File inputFile, String sheetName) 
	{
		long startTime = System.currentTimeMillis();
		
		// For storing data into CSV files
		StringBuffer data = new StringBuffer();
		HSSFWorkbook workbook = null;
		try 
		{
			FileOutputStream fos = new FileOutputStream(outputCSVFile);

			// Get the workbook object for XLS file
			workbook = new HSSFWorkbook(new FileInputStream(inputFile));
			
			// Get sheet from the workbook
			HSSFSheet sheet = workbook.getSheet(sheetName);
			if (sheet == null) {
				System.out.println("Not a valid sheet");
				fos.close();
				throw new RuntimeException();
			}
			Cell cell;
			Row row;

			// Iterate through each row from sheet
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) 
			{
				//If sheet is Base Data it needs an empty place 
				//holder for AUTO_INCREMENT on id
				if (sheetName.equals("Base Data")) {
					data.append(";");
				}

				row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) 
				{
					cell = cellIterator.next();

					switch (cell.getCellType()) 
					{
					case Cell.CELL_TYPE_BOOLEAN:
						data.append(cell.getBooleanCellValue() + ";");
						break;

					case Cell.CELL_TYPE_NUMERIC:{
						if (DateUtil.isCellDateFormatted(cell)) {
							java.util.Date dt = cell.getDateCellValue();

							java.text.SimpleDateFormat sdf = 
									new java.text.SimpleDateFormat("YYYY-MM-DD HH:MM:SS");

							String currentTime = sdf.format(dt);
							data.append(currentTime+ ";");
							break;
						} else {
							long d = (long)cell.getNumericCellValue();
							data.append(d + ";");
							break;
						}
					}

					case Cell.CELL_TYPE_STRING:
						if (cell.getStringCellValue().contains("(null)")) {
							data.append("\\N;");
						}
						else{
							data.append(cell.getStringCellValue() + ";");
						}
						break;

					case Cell.CELL_TYPE_BLANK:
						data.append("");
						break;

					default:
						data.append(cell + ";");
					}
				}
				data.append('\n'); 
			}

			fos.write(data.toString().getBytes());
			fos.close();
	//		workbook.close();

			long endTime = System.currentTimeMillis();

			System.out.println("Converting "+ sheetName +" to .csv took: "+ (endTime - startTime) +" milliseconds");
			
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return outputCSVFile;

	}
}
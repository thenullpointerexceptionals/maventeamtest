package com.ait.team;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class WriteToDatabase {

	public static boolean importDataToDB(Connection conn, String csvFile, String tableName) {

		Statement stmt;
		String query;

		try
		{
			stmt = conn.createStatement(
					ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			query = " LOAD DATA LOCAL INFILE '" + csvFile +
					"' INTO TABLE "+ tableName +
					" FIELDS TERMINATED BY \';\' " +
					" LINES TERMINATED BY \'\\n\'"
					+ "IGNORE 1 LINES";

			stmt.executeUpdate(query);
			
			System.out.println("Data imported to table " + tableName + "\n");
			return true;

		}
		catch(SQLException e)
		{
			e.printStackTrace();
			stmt = null;
			return false;
		}
	}
}
package com.ait.team;


import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MainConverter {

public static final Map<String, String> sheetTableMap; 
	
	static
	{
		sheetTableMap = new HashMap<String, String>();
		sheetTableMap.put("MCC - MNC Table", "mcc_mnc_table");
		sheetTableMap.put("UE Table", "ue_table");
		sheetTableMap.put("Event-Cause Table", "event_causetest");
		sheetTableMap.put("Base Data", "base_data");
		sheetTableMap.put("Failure Class Table", "failure_class");
	}
	
	private static File convertedFile;

	public static void main(String[] args) throws SQLException {
		
		ConvertToCSV converter = new ConvertToCSV();
		File xlsFIle = new File("excel/AIT Group Project - Sample Dataset.xls");

		Connection connection = ConnectionHelper.getConnection();

		//Timer for writing data
		long startTime = System.currentTimeMillis();

		for (Map.Entry entry : sheetTableMap.entrySet()) {
			convertedFile = converter.convertFile(xlsFIle, entry.getKey().toString());
			WriteToDatabase.importDataToDB(connection, convertedFile.getName(), entry.getValue().toString());
		}
		long endTime = System.currentTimeMillis();

		System.out.println("Total time: " + (endTime - startTime) + " milliseconds");

		//deletes the file once its used..
		if(convertedFile.delete()){
			System.out.println(convertedFile.getName() + " has been deleted!");
		}else{
			System.out.println("Delete operation failed.");
		}

	}
}